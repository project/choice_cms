<?php

/**
 * @file
 * The Choice CMS General Settings functionality.
 */

/**
 * Quantcast Choice CMS Admin Settings Form.
 *
 * General Settings Form.
 *
 * @param $form
 *   An array contianing the form settings and field settings.
 * @param $form_state
 *   An associative array containing the current state of the form.
 *
 * @return
 *   Drupal Form Array
 */
function choice_cms_settings_general_form( $form, &$form_state ) {

  $choice_selected_vendors_default = variable_get( 'choice_selected_vendors', '' );
  $choice_publisher_logo_fid = variable_get( 'publisher_logo_fid', '' );

  $element['#file'] = $choice_publisher_logo_fid ? file_load($choice_publisher_logo_fid) : FALSE;
  $choice_publisher_logo_uri = $element['#file']->uri ? $element['#file']->uri : '';
  $file_name = isset( $element['#file']->filename ) ? $element['#file']->filename : '';

  // Vendor form definition.
  $form = array(
    '#attributes' => array(
      'class'     => array(
        'general-settings-form',
        'choice-cms',
      ),
    ),
    '#attached'   => array(
      'js'        => array(
        drupal_get_path( 'module', 'choice_cms' ) . '/js/admin/choice-cms.admin.min.js',
      )
    ),
  );

  // Add Choice Publisher/Site Information fieldset
  $form['choice_publisher'] = array(
    '#type'        => 'fieldset',
    '#title'       => t( 'Publisher/Site Information' ),
    '#description' => t( 'Publisher information, displayed in the frontend UI.' ),
    '#weight'      => 1,
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  // Add Publisher Name input.
  $form['choice_publisher']['publisher_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Publisher Name' ),
    '#description'   => t( 'The name of the company using the CMP, to be displayed in the UI.' ),
    '#default_value' => variable_get( 'choice_publisher_name', '' ),
    '#required'      => TRUE,
    '#weight'        => 1,
  );

  $form['choice_publisher']['publisher_logo'] = array(
    '#type'        => 'file',
    '#title'       => t( 'Publisher Logo' ),
    '#description' => t( 'The logo for the company using the CMP, to be displayed in the UI.' ),
    '#required'    => FALSE,
    '#weight'      => 2,
  );

  $form['choice_publisher']['publisher_logo_fid'] = array(
    '#type'          => 'hidden',
    '#default_value' => $choice_publisher_logo_fid,
  );

  if ( $choice_publisher_logo_fid && $element['#file'] ) {
    $arbitrary_array = array(
      'path'  => file_create_url( $choice_publisher_logo_uri ),
      'alt'   => 'Publisher Logo',
      'title' => 'Publisher Logo',
    );

    if ( $choice_publisher_logo_uri ) {
      $arbitrary_array = array(
        'path'       => file_create_url( $choice_publisher_logo_uri ),
        'alt'        => 'Publisher Logo',
        'title'      => 'Publisher Logo',
        'attributes' => array( 'class' => 'publisher-logo', 'id' => 'publisher_logo' ),
      );
      $form['choice_publisher']['publisher_logo_display_2'] = array(
        '#type'   => 'markup',
        '#markup' => theme( 'image', $arbitrary_array ),
        '#weight' => 9,
      );
    }
  }

  // Add Choice Languages fieldset
  $form['choice_lang'] = array(
    '#type'        => 'fieldset',
    '#title'       => t( 'Language Options' ),
    '#description' => t( 'Choice CMP language options.' ),
    '#weight'      => 2,
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  // Add Active Language checkboxes.
  $form['choice_lang']['lang_active'] = array(
    '#type'        => 'checkboxes',
    '#title'       => t( 'Active Languages' ),
    '#description' => t( 'The module will detect the users browser language and if there is an active matching language that language will be displayed in the frontend CMS.' ),
    '#options'     => array(
      'da'         => t( 'DA - Danish' ),
      'de'         => t( 'DE - German' ),
      'el'         => t( 'EL - Greek' ),
      'en'         => t( 'EN - English' ),
      'es'         => t( 'ES - Spanish' ),
      'fi'         => t( 'FI - Finnish' ),
      'fr'         => t( 'FR - French' ),
      'hu'         => t( 'HU - Hungarian' ),
      'it'         => t( 'IT - Italian' ),
      'nl'         => t( 'NL - Dutch' ),
      'pl'         => t( 'PL - Polish' ),
      'pt'         => t( 'PT - Portuguese' ),
      'ro'         => t( 'RO - Romanian' ),
      'sk'         => t( 'SK - Slovak' ),
      //'ru'         => t( 'RU - Russian' ),
      //'ar'         => t( 'AR - Arabic' ),
      //'fa'         => t( 'FA - Farsi' ),
      //'sl'         => t( 'SL - Slovenian' ),
      //'sv'         => t( 'SV - Swedish' ),
      //'lt'         => t( 'LT - Lithuanian' ),
      //'lv'         => t( 'LV - Latvian' ),
      //'mt'         => t( 'MT - Maltese' ),
      //'hr'         => t( 'HR - Croatian' ),
      //'bg'         => t( 'BG - Bulgarian' ),
      //'cs'         => t( 'CS - Czech' ),
      //'et'         => t( 'ET - Estonian' ),
    ),
    '#default_value' => variable_get( 'choice_lang_active', array() ),
    '#required'    => TRUE,
    '#weight'      => 1,
  );

  // Add Default Language select.
  $form['choice_lang']['lang_default'] = array(
    '#type'        => 'select',
    '#title'       => t( 'Default Language' ),
    '#description' => t( 'The default language will be shown to users that have a browser detected languge that is not an activated language.' ),
    '#options'     => array(
      'da'         => t( 'DA - Danish' ),
      'de'         => t( 'DE - German' ),
      'el'         => t( 'EL - Greek' ),
      'en'         => t( 'EN - English' ),
      'es'         => t( 'ES - Spanish' ),
      'fi'         => t( 'FI - Finnish' ),
      'fr'         => t( 'FR - French' ),
      'hu'         => t( 'HU - Hungarian' ),
      'it'         => t( 'IT - Italian' ),
      'nl'         => t( 'NL - Dutch' ),
      'pl'         => t( 'PL - Polish' ),
      'pt'         => t( 'PT - Portuguese' ),
      'ro'         => t( 'RO - Romanian' ),
      'sk'         => t( 'SK - Slovak' ),
      //'ru'         => t( 'RU - Russian' ),
      //'ar'         => t( 'AR - Arabic' ),
      //'fa'         => t( 'FA - Farsi' ),
      //'sl'         => t( 'SL - Slovenian' ),
      //'sv'         => t( 'SV - Swedish' ),
      //'lt'         => t( 'LT - Lithuanian' ),
      //'lv'         => t( 'LV - Latvian' ),
      //'mt'         => t( 'MT - Maltese' ),
      //'hr'         => t( 'HR - Croatian' ),
      //'bg'         => t( 'BG - Bulgarian' ),
      //'cs'         => t( 'CS - Czech' ),
      //'et'         => t( 'ET - Estonian' ),
    ),
    '#default_value' => variable_get( 'choice_lang_default', '' ),
    '#required'    => TRUE,
    '#weight'      => 2,
  );

  // Add Choice UI fieldset
  $form['choice_ui_display'] = array(
    '#type'        => 'fieldset',
    '#title'       => t( 'Display UI Options' ),
    '#description' => t( 'Choice CMP display UI options.' ),
    '#weight'      => 3,
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  // Add Display UI select.
  $form['choice_ui_display']['ui_display'] = array(
    '#type'        => 'select',
    '#title'       => t( 'Display UI' ),
    '#description' => '
                      <ul>
                        <li>' . t( "EU Only -  UI will be displayed to users in the EU when consent is required." ) . '</li>
                        <li>' . t( "Always - UI will be displayed to everyone when consent is required." ) . '</li>
                        <li>' . t( "Never - UI will never be shown." ) . '</li>
                      </ul>',
    '#options'     => array(
      'inEU'       => t( 'EU Only' ),
      'always'     => t( 'Always' ),
      'never'      => t( 'Never' ),
    ),
    '#default_value' => variable_get( 'choice_ui_display', '' ),
    '#required'    => TRUE,
    '#weight'      => 3,
  );
  // Add Display UI select.
  $form['choice_ui_display']['ui_layout'] = array(
    '#type'        => 'radios',
    '#title'       => t( 'UI Layout' ),
    '#description' => t( '' ),
    '#options'     => array(
      'popup'      => t( 'Popup - The Choice CMS consent displayed in the center of the screen.' ),
      'banner'     => t( 'Banner - The Choice CMS consent is displayed at the bottom of the screen.' ),
    ),
    '#default_value' => variable_get( 'choice_ui_layout', 'popup' ),
    '#required'    => TRUE,
    '#weight'      => 4,
  );

  // Add Minimum Number of Days Between UI Displays input.
  $form['choice_ui_display']['min_days_between_display'] = array(
    '#type'        => 'textfield',
    '#title'       => t( 'Minimum Number of Days Between UI Displays' ),
    '#description' => t( 'Determines how often the CMP will check for an updated vendor list and get updated consent using the ui if there is an updated list.' ),
    '#required'    => TRUE,
    '#default_value' => variable_get( 'choice_min_days_between_display', '' ),
    '#attributes'  => array(
      'type'       => 'number',
    ),
    '#weight'      => 4,
  );

  // Add Choice Vendors fieldset
  $form['choice_vendors'] = array(
    '#type'        => 'fieldset',
    '#title'       => t( 'Choice Vendors' ),
    '#description' => t( 'Select specific vendors to reqeust consent for.  If no specific vendors are selected, all vendors will show.' ),
    '#weight'      => 4,
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  // Add Choice All Vendors field with placeholder options that will be replaced with data from window.__cmp()
  $form['choice_vendors']['all_vendors'] = array(
    '#title'  => 'Select Your Vendors',
    '#type'          => 'checkboxes',
    '#attributes' => array(
      'class' => array(
        'loading-vendors'
      ),
      'id' => 'all_vendor_list'
    ),
    '#weight' => 100,
    '#options' => generate_placeholder_selected_vender_options(), // We are adding a bunch of placehoder items with values like 1-2000, these will be replaced by dymanically generated items from the choice cmp current vendor list.
  );

  // Add Choice Selected Vendors, these are the vendors previously selected by the admin user.
  $form['choice_vendors']['selected_vendors'] = array(
    '#type'          => 'hidden',
    '#default_value' => $choice_selected_vendors_default,
    '#attributes' => array( 'id' => 'selected_vendor_list' ),
  );

  // Add vendors loaded
  $form['choice_vendors']['vendors_loaded'] = array(
    '#type'          => 'hidden',
    '#default_value' => 'false',
    '#attributes' => array( 'id' => 'vendors_loaded' ),
  );

  // Add the submit button to the form.
  $form['submit_button'] = array(
    '#type'   => 'submit',
    '#value'  => t( 'Update General Settings' ),
    '#weight' => 100,
  );

  return $form;

}

/**
 * Implements hook_validate().
 *
 * Choice CMS Settings General form validate.
 *
 */
function choice_cms_settings_general_form_validate( $form, &$form_state ) {

  $values = isset( $form_state['values'] ) && ! empty( isset( $form_state['values'] ) ) ? $form_state['values'] : array();

  // Validate the min_days_between_display value.
  $ui_min_days_between_display    = isset( $values['min_days_between_display'] ) ? $values['min_days_between_display'] : '';
  if ( ! is_numeric( $ui_min_days_between_display ) ) {
    form_set_error( 'min_days_between_display', t( '"Minimum Number of Days Between UI Displays" must be an integer value.' ) );
  }

  // Publisher Logo file upload validation.
  $file = file_save_upload( 'publisher_logo', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array( 'png jpg jpeg' ),
  ) );

  if ( $file ) {
    if ( $file = file_move( $file, 'public://' ) ) {
      $form_state['values']['publisher_logo'] = $file;
    }
    else {
      form_set_error( 'publisher_logo', t( 'Failed to write the uploaded file the site\'s file folder.' ) );
    }
  }

}

/**
 * Implements hook_submit().
 *
 * Choice CMS Settings General form submit.
 *
 */
function choice_cms_settings_general_form_submit( $form, &$form_state ) {

  $values = isset( $form_state['values'] ) && ! empty( isset( $form_state['values'] ) ) ? $form_state['values'] : array();
  $current_choice_selected_vendors = variable_get( 'choice_selected_vendors', '' );

  $pub_name                       = isset( $values['publisher_name'] )           ? $values['publisher_name']           : '';
  $pub_logo_url                   = isset( $values['publisher_logo_url'] )       ? $values['publisher_logo_url']       : '';
  $pub_logo                       = isset( $values['publisher_logo'] )           ? $values['publisher_logo']           : '';
  $lang_default                   = isset( $values['lang_default'] )             ? $values['lang_default']             : '';
  $lang_active                    = isset( $values['lang_active'] )              ? $values['lang_active']              : '';
  $ui_display                     = isset( $values['ui_display'] )               ? $values['ui_display']               : '';
  $ui_layout                      = isset( $values['ui_layout'] )                ? $values['ui_layout']                : '';
  $ui_min_days_between_display    = isset( $values['min_days_between_display'] ) ? $values['min_days_between_display'] : '';

  variable_set( 'choice_publisher_name', $pub_name);
  variable_set( 'choice_lang_active', $lang_active);
  variable_set( 'choice_lang_default', $lang_default);
  variable_set( 'choice_ui_display', $ui_display);
  variable_set( 'choice_ui_layout', $ui_layout);
  variable_set( 'choice_min_days_between_display', floor( $ui_min_days_between_display ) );

  if ( $pub_logo ) {
    unset( $form_state['values']['publisher_logo'] );
    $pub_logo->status = FILE_STATUS_PERMANENT;
    file_save($pub_logo);
    drupal_set_message(t( 'The form has been submitted and the image has been saved, filename: @filename.', array( '@filename' => $pub_logo->filename)));

    $fid = isset( $pub_logo->fid ) ? $pub_logo->fid : '';

    variable_set( 'publisher_logo_fid', $fid );
  }

  /////
  // Handle Vendor Selection.
  /////
  $all_vendors    = isset( $values['all_vendors'] )    ? $values['all_vendors']    : '';
  $vendors_loaded = isset( $values['vendors_loaded'] ) ? $values['vendors_loaded'] : '';

  $save_selected_vendors = '';
  $cnt = 1;
  if ( '' !== $all_vendors && is_array( $all_vendors ) ) {
    $array_cnt = count( $all_vendors );
    foreach ( $all_vendors as $value ) {
      if ( $value > 0 ) {
        $save_selected_vendors .= $value;
        if ( $array_cnt > $cnt ) {
          $save_selected_vendors .= ",";
        }
      }
      $cnt++;
    }

    $save_selected_vendors = rtrim( $save_selected_vendors, "," );
  }

   // Only update the choice_selected_vendors and .well-known/pubvendors.json file if the venors were loaded from the choice cmp.
  if ( 'true' === $vendors_loaded) {

    // Only update the choice_selected_vendors and .well-known/pubvendors.json file if there was as update to the selected vendors.
    if ( $current_choice_selected_vendors != $save_selected_vendors ) {

      // Save the pubvendors.json file with the new vendor ids.
      $file_save_successful = save_vendor_file( $save_selected_vendors );

      // If the file save was successful, then update the choice_selected_vendors settings value.
      if ( $file_save_successful ) {

        // Update choice_selected_vendors with the newly selected vendors
        variable_set( 'choice_selected_vendors', $save_selected_vendors);

      }

    }

  }

}

/**
 * Generate placeholder checkbox options for the 'all_vendors' field.
 * - These options are re
 *
 * @since   1.0.0
 */
function generate_placeholder_selected_vender_options() {

  $ret = array();
  for ( $x = 1; $x <= 800; $x++ ) {
    $ret[$x] = t( 'Placeholder' );
  }

  return $ret;

}

/**
 * Start the save process for the .well-known/pubvendors.json file.
 *
 * @since   1.0.0
 * @param   string $vendor_ids  A comma seperated string value containing all of the choice cms vendor ids to save to the pubvendors.json file.
 */
function save_vendor_file( $vendor_ids ) {

  $file_save_successful = choice_cms_vendors_update( $vendor_ids );

  // Return the file save status.
  return $file_save_successful;

}

/**
 * Generate the contents to save in the .well-known/pubvendors.json file.
 *
 * @since   1.0.0
 * @param   string $vendor_ids  A comma seperated string value containing all of the choice cms vendor ids to save to the pubvendors.json file.
 */
function generate_pubvendors_file( $vendor_ids = '' ) {

  $vendors_ids_array = explode( ',', $vendor_ids );

  // Build the vendor array in the necessary structure for pubvendors.json
  foreach ( $vendors_ids_array as $value ) {
    $vendors[] = array(
      "id" => $value
    );
  }

  // Get the vendor list version number.
  $global_vendor_list_version = get_vendor_list_version();

  if ( ! $global_vendor_list_version['success'] ) {
      return $global_vendor_list_version;
  }

  $choice_vendor_list_version = variable_get( 'choice_vendor_list_version', 1 );

  // Update the qc_choice_vendor_list_version value to the next int for this updated save.
  $choice_vendor_list_version++;

  return array(
    "publisherVendorsVersion" => PUBLISHER_VENDORS_VERSION,
    "version"                 => $choice_vendor_list_version,
    "globalVendorListVersion" => $global_vendor_list_version['version'],
    "updatedAt"               => gmdate( "Y-m-d\TH:i:s\Z" ),
    "vendors"                 => $vendors
  );

}

/**
 * Update the choice-cms/.well-known/pubvendors.json file when the selected vendors are values are updated.
 *
 * @since   1.0.0
 * @param   string $vendors   A comma seperated string value containing all of the choice cms vendor ids to save to the pubvendors.json file.
 */
function choice_cms_vendors_update( $vendors ) {

  $save = FALSE;
  $pubvendors_path = CHOICE_VENDOR_FILE_URI . CHOICE_VENDOR_FILE;

  // Generate/Get the pubvendors.json file contents, and json_encode the array.
  $content = json_encode( generate_pubvendors_file( $vendors ) );

  if ( $pubvendors_path ) {
    $save = file_save_data( $content, $pubvendors_path, FILE_EXISTS_REPLACE );
  }

  // Notify the user of the save status.
  if ( $save ) {

    // If the save was successfuly, update the choice_vendor_list_version counter value.
    $choice_vendor_list_version = variable_get( 'choice_vendor_list_version', 1 );
    $choice_vendor_list_version++;
    variable_set( 'choice_vendor_list_version', $choice_vendor_list_version );

    drupal_set_message( t( 'The Quantcast Choice CMS pubvendors.json file (@file_location) has been successfully updated with your new vendor selections.', array( '@file_location' => $pubvendors_path ) ), 'status' );
  }
  else {
    drupal_set_message( t( 'There was a problem updating the Quantcast Choice CMS pubvendors.json (@file_location) file with your new vendor selections.  Please review your selections and try again.', array( '@file_location' => $pubvendors_path ) ), 'error' );
  }

  // Return the outcome of the file save from file_save_data call.
  return $save;

}

/**
 * Retrieve the current vendor list version from: https://vendorlist.consensu.org/vendorlist.json.
 *
 * @since    1.0.0
 */
function get_vendor_list_version() {
  $ch = curl_init();
  curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
  curl_setopt( $ch, CURLOPT_URL, 'https://vendorlist.consensu.org/vendorlist.json' );
  $result = curl_exec( $ch );
  if ( curl_errno( $ch ) ) {
    curl_close( $ch );
    // curl error, return array with error data.
    return array(
      'success' => FALSE,
      'msg'     => '<h3>' . t( 'Somehting went wrong.' ) . '</h3><p><strong>' . t( 'The pubvendors.json was NOT updated.' ) . '</strong></p><p>' . t( 'We were unable to retrieve the vendor list version number, please try saving again.' ) . '</p>',
      'error'   => curl_error( $ch ),
      'version' => 0
    );
  }
  else {
    // check the HTTP status code of the request
    $result_status = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
    curl_close( $ch );
    if ( $result_status != 200 ) {
      // http error, return array with error data.
      return array(
        'success' => FALSE,
        'msg'     => '<h3>' . t( 'Somehting went wrong.' ) . '</h3><p><strong>' . t( 'The pubvendors.json was NOT updated.' ) . '</strong></p><p>' . t( 'We were unable to retrieve the vendor list version number, please try saving again.' ) . '</p>',
        'error'   => t( 'HTTP status code:' ) . ' ' . $result_status,
        'version' => 0
      );
    }
    else{
      $obj = json_decode( $result );
      if ( ! property_exists( $obj, 'vendorListVersion' ) ) {
        // missing vendorListVersion error, return array with error data.
        return array(
          'success' => FALSE,
          'msg'     => '<h3>' . t( 'Somehting went wrong.' ) . '</h3><p><strong>' . t( 'The pubvendors.json was NOT updated.' ) . '</strong></p><p>' . t( 'We were unable to retrieve the vendor list version number, please try saving again.' ) . '</p>',
          'error'   => t( 'Unknown error.' ),
          'version' => 0
        );
      }
      else {
        // success, return the data.
        return array(
          'success' => TRUE,
          'msg'     => t( 'Vendor List Version successfully retrieved.' ),
          'error'   => t( 'None.' ),
          'version' => $obj->vendorListVersion
        );
      }
    }
  }
}
