<?php

/**
 * @file
 * The Choice CMS Vendor Screen Settings functionality.
 */

/**
 * Quantcast Choice CMS Admin Settings Form.
 *
 * Purpose Screen Settings Form.
 *
 * @param $form
 *   An array contianing the form settings and field settings.
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @param $lang
 *   A string that contains the arg(4) element from the url, passed in from choice_cms_menu()
 * @return
 *   Drupal Form Array
 */
function choice_cms_settings_vendor_screen_form( $form, &$form_state, $lang ) {

  // Get the currently active Choice CMS items
  $active_languages = variable_get( 'choice_lang_active', array() );
  $lang = strtolower( $lang );

  // Vendor form definition.
  $form = array(
    '#attributes' => array(
      'class'     => array(
        'vendor-screen-form',
        'choice-cms',
        'waiting-for-values',
      ),
      'data-choice-lang' => strtolower( $lang ),
    ),
    '#attached'   => array(
      'js'        => array(
        drupal_get_path( 'module', 'choice_cms' ) . '/js/admin/choice-cms.admin.min.js',
      )
    ),
  );

  // Add the form header section.
  $form['top'] = array(
    '#type'   => 'markup',
    '#markup' => '
      <div class="form-header">
        <h2>Vendor Screen Settings - ' . strtoupper( $lang ) . '</h2>
      </div>
    ',
    '#weight' => -1,
  );

  // If there are no active Choice CMS languages.
  if ( empty( $active_languages ) ) {

    // Add a message for display.
    $form['msg'] = array(
      '#type'   => 'markup',
      '#markup' => "<p>" . t( "You don't have any active Choice CMS languages selected." ) . "</p><p>" . t( "Head over to the <a href='@url'>Choice CMS General Settings</a> and activate your languages", array( "@url" => url( "/admin/config/choice-cms/general", array("fragment" => "edit-choice-lang" ) ) ) ) . "</p>",
      '#weight' => 2,
    );

    // Return the form with the no active languages message.
    return $form;

  }

  // The user has active languages, however the current page attempting to be viewed is not one of those active languages.
  elseif ( ! in_array( $lang, $active_languages, TRUE ) )  {

    $form['msg'] = array(
      '#type'   => 'markup',
      '#markup' => '<p>' . t( "@lang is not an active Choice CMS language.", array( '@lang' => strtoupper( $lang ) ) ) . '</p><p>' . t( 'Inactive Choice CMS languages can be activated on the <a href="@url">General Settings</a> page.', array( '@url' => url( '/admin/config/choice-cms/general', array('fragment' => 'edit-choice-lang' ) ) ) ) . '</p>',
      '#weight' => 2,
    );

    // The language being viewed is not an active language, return the form.
    return $form;

  }

  //////
  // The user is viewing a language settings page of a currently active Choice CMS language.
  // - Build the plugin settings for the vendor screen.
  //////

  // Get the currently stored values.
  $choice_vendor_screen_title              = variable_get( 'choice_vendor_screen_title', json_encode( array() ) );
  $choice_vendor_screen_body               = variable_get( 'choice_vendor_screen_body', json_encode( array() ) );
  $choice_vendor_screen_accept_all         = variable_get( 'choice_vendor_screen_accept_all', json_encode( array() ) );
  $choice_vendor_screen_reject_all         = variable_get( 'choice_vendor_screen_reject_all', json_encode( array() ) );
  $choice_vendor_screen_purpose            = variable_get( 'choice_vendor_screen_purpose', json_encode( array() ) );
  $choice_vendor_screen_cancel             = variable_get( 'choice_vendor_screen_cancel', json_encode( array() ) );
  $choice_vendor_screen_save_and_exit      = variable_get( 'choice_vendor_screen_save_and_exit', json_encode( array() ) );

  // Decode the json returned from the above variable_get calls.
  $choice_vendor_screen_title          = isset( $choice_vendor_screen_title ) && ! empty( $choice_vendor_screen_title ) ? json_decode( $choice_vendor_screen_title, TRUE ) : array();
  $choice_vendor_screen_body           = isset( $choice_vendor_screen_body ) && ! empty( $choice_vendor_screen_body ) ? json_decode( $choice_vendor_screen_body, TRUE ) : array();
  $choice_vendor_screen_accept_all     = isset( $choice_vendor_screen_accept_all ) && ! empty( $choice_vendor_screen_accept_all ) ? json_decode( $choice_vendor_screen_accept_all, TRUE ) : array();
  $choice_vendor_screen_reject_all     = isset( $choice_vendor_screen_reject_all ) && ! empty( $choice_vendor_screen_reject_all ) ? json_decode( $choice_vendor_screen_reject_all, TRUE ) : array();
  $choice_vendor_screen_purpose        = isset( $choice_vendor_screen_purpose ) && ! empty( $choice_vendor_screen_purpose ) ? json_decode( $choice_vendor_screen_purpose, TRUE ) : array();
  $choice_vendor_screen_cancel         = isset( $choice_vendor_screen_cancel ) && ! empty( $choice_vendor_screen_cancel ) ? json_decode( $choice_vendor_screen_cancel, TRUE ) : array();
  $choice_vendor_screen_save_and_exit  = isset( $choice_vendor_screen_save_and_exit ) && ! empty( $choice_vendor_screen_save_and_exit ) ? json_decode( $choice_vendor_screen_save_and_exit, TRUE ) : array();

  // Add a loader to give the user feedback when the form is waiting for data.
  $form['loader'] = array(
    '#type'          => 'markup',
    '#markup'        => '<div class="loader"></div>',
    '#weight'        => -999,
  );

  // Add the current language to the form as a hidden field.
  $form['choice_vendor_screen']['lang'] = array(
    '#type'          => 'hidden',
    '#default_value' => $lang,
    '#weight'        => -100,
  );

  // Add Vendor Screen Title.
  $form['choice_vendor_screen']['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Title Text' ),
    '#description'   => t( 'The title text on the vendors consent page to be displayed in the ui.' ),
    '#default_value' => isset( $choice_vendor_screen_title[$lang] ) ? $choice_vendor_screen_title[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 5,
  );

  // Add Vendor Screen Body Text.
  $form['choice_vendor_screen']['body'] = array(
    '#type'          => 'textarea',
    '#title'         => t( 'Body Text' ),
    '#description'   => t( 'The main body text on the vendors consent page to be displayed in the ui.' ),
    '#default_value' => isset( $choice_vendor_screen_body[$lang] ) ? $choice_vendor_screen_body[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 10,
  );

  // Add Vendor Screen Accept All Button Text.
  $form['choice_vendor_screen']['accept_all'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Accept All Button Text'),
    '#description'   => t( 'The accept all vendor consents button text on the vendors consent page to be displayed in the ui. When clicked, all of the vendors consent toggles are set to on.'),
    '#default_value' => isset( $choice_vendor_screen_accept_all[$lang] ) ? $choice_vendor_screen_accept_all[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 15,
  );

  // Add Vendor Screen Reject All Button Text.
  $form['choice_vendor_screen']['reject_all'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Reject All Button Text' ),
    '#description'   => t( 'The reject all vendor consents button text on the vendors consent page to be displayed in the ui. When clicked, all of the vendors consent toggles are set to off.'),
    '#default_value' => isset( $choice_vendor_screen_reject_all[$lang] ) ? $choice_vendor_screen_reject_all[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 20,
  );

  // Add Vendor Screen Purpose Button Text.
  $form['choice_vendor_screen']['purpose'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Purposes Link Text' ),
    '#description'   => t( 'The purpose link text on the vendors consent page to be displayed in the ui. When clicked, the ui updates to display the purpose consent page.' ),
    '#default_value' => isset( $choice_vendor_screen_purpose[$lang] ) ? $choice_vendor_screen_purpose[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 25,
  );

  // Add Vendor Screen Cancel Button Text.
  $form['choice_vendor_screen']['cancel'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Cancel Button Text' ),
    '#description'   => t( 'The cancel link text on the vendors consent page to be displayed in the ui. When clicked, the ui updates to display the initial global consent page.' ),
    '#default_value' => isset( $choice_vendor_screen_cancel[$lang] ) ? $choice_vendor_screen_cancel[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 30,
  );

  // Add Vendor Screen Save and Exit Button Text.
  $form['choice_vendor_screen']['save_and_exit'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Save and Exit Button Text' ),
    '#description'   => t( 'The save consent and exit the ui button text on the vendors consent page to be displayed in the ui.' ),
    '#field_suffix'  => '',
    '#default_value' => isset( $choice_vendor_screen_save_and_exit[$lang] ) ? $choice_vendor_screen_save_and_exit[$lang] : '',
    '#required'      => TRUE,
    '#weight'        => 35,
  );

  // Add the form submit button.
  $form['submit_button'] = array(
    '#type'   => 'submit',
    '#value'  => t( 'Update Vendor Screen Settings' ) . ' - ' . strtoupper( $lang ),
    '#weight' => 1000,
  );

  return $form;

}

/**
 * Implements hook_submit().
 */
function choice_cms_settings_vendor_screen_form_submit( $form, &$form_state ) {

  $active_languages = variable_get( 'choice_lang_active', array() );
  $values           = isset( $form_state['values'] ) && ! empty( isset( $form_state['values'] ) ) ? $form_state['values'] : array();

  // Get the current stored values for the purpose screen
  $current_choice_vendor_screen_title               = json_decode( variable_get( 'choice_vendor_screen_title', json_encode( array() ) ), TRUE );
  $current_choice_vendor_screen_body                = json_decode( variable_get( 'choice_vendor_screen_body', json_encode( array() ) ), TRUE );
  $current_choice_vendor_screen_accept_all          = json_decode( variable_get( 'choice_vendor_screen_accept_all', json_encode( array() ) ), TRUE );
  $current_choice_vendor_screen_reject_all          = json_decode( variable_get( 'choice_vendor_screen_reject_all', json_encode( array() ) ), TRUE );
  $current_choice_vendor_screen_purpose             = json_decode( variable_get( 'choice_vendor_screen_purpose', json_encode( array() ) ), TRUE );
  $current_choice_vendor_screen_cancel              = json_decode( variable_get( 'choice_vendor_screen_cancel', json_encode( array() ) ), TRUE );
  $current_choice_vendor_screen_save_and_exit       = json_decode( variable_get( 'choice_vendor_screen_save_and_exit', json_encode( array() ) ), TRUE );

  // Get the values submitted for the intitial screen form
  $lang                                             = isset( $values['lang'] )            ? $values['lang']            : '';
  $submitted_choice_vendor_screen_title             = isset( $values['title'] )           ? $values['title']           : '';
  $submitted_choice_vendor_screen_body              = isset( $values['body'] )            ? $values['body']            : '';
  $submitted_choice_vendor_screen_accept_all        = isset( $values['accept_all'] )      ? $values['accept_all']      : '';
  $submitted_choice_vendor_screen_reject_all        = isset( $values['reject_all'] )      ? $values['reject_all']      : '';
  $submitted_choice_vendor_screen_purpose           = isset( $values['purpose'] )         ? $values['purpose']         : '';
  $submitted_choice_vendor_screen_cancel            = isset( $values['cancel'] )          ? $values['cancel']          : '';
  $submitted_choice_vendor_screen_save_and_exit     = isset( $values['save_and_exit'] )   ? $values['save_and_exit']   : '';

  // Only save values if the language being saved is an active language.
  if ( in_array( strtolower( $lang ), $active_languages ) )  {

    // Update the current values, for the specific language submitted
    // Encode the array into json and save the field data
    $current_choice_vendor_screen_title[$lang] = $submitted_choice_vendor_screen_title;
    variable_set( 'choice_vendor_screen_title', json_encode( $current_choice_vendor_screen_title ) );

    $current_choice_vendor_screen_body[$lang] = $submitted_choice_vendor_screen_body;
    variable_set( 'choice_vendor_screen_body', json_encode( $current_choice_vendor_screen_body ) );

    $current_choice_vendor_screen_accept_all[$lang] = $submitted_choice_vendor_screen_accept_all;
    variable_set( 'choice_vendor_screen_accept_all', json_encode( $current_choice_vendor_screen_accept_all ) );

    $current_choice_vendor_screen_reject_all[$lang] = $submitted_choice_vendor_screen_reject_all;
    variable_set( 'choice_vendor_screen_reject_all', json_encode( $current_choice_vendor_screen_reject_all ) );

    $current_choice_vendor_screen_purpose[$lang] = $submitted_choice_vendor_screen_purpose;
    variable_set( 'choice_vendor_screen_purpose', json_encode( $current_choice_vendor_screen_purpose ) );

    $current_choice_vendor_screen_cancel[$lang] = $submitted_choice_vendor_screen_cancel;
    variable_set( 'choice_vendor_screen_cancel', json_encode( $current_choice_vendor_screen_cancel ) );

    $current_choice_vendor_screen_save_and_exit[$lang] = $submitted_choice_vendor_screen_save_and_exit;
    variable_set( 'choice_vendor_screen_save_and_exit', json_encode( $current_choice_vendor_screen_save_and_exit ) );

  }

}
