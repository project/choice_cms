<?php

/**
 * @file
 * Admin page callbacks for the choice-cms module.
 */

/**
 * Menu callback; present an administrative comment listing.
 */
function choice_cms_settings_initial_screen( $lang, $screen = '' ) {
  if ( empty( $screen ) || 'initial-screen' === strtolower( $screen ) ) {
    return drupal_get_form('choice_cms_settings_initial_screen_form', $lang );
  }

  else {
    return $block['content'] = '<div>NOTHING HERE RIGHT NOW</div>';
  }
}

/**
 * Create a list of language pages for display in the admin.
 *
 * @since 1.0.0
 */
function choice_cms_lang_page_list() {

  // Get the list of currently active languages for the Choice CMS.
  $active_langs = variable_get( 'choice_lang_active', array() );

  // Set the active/inactive language item arrays to empty.
  $active_lang_items = array();
  $inactive_lang_items = array();


  foreach ($active_langs as $key => $lang) {
    if ( $lang ) {
      // Add language items to the active language items array.
      $active_lang_items[] = t('<a href="@url">Edit @lang Settings</a>',
        array(
          '@url' => url('/admin/config/choice-cms/lang/' . $key . '/' ),
          '@lang' => strtoupper( $key ),
        )
      ) . '<div class="description">' . t('Edit the Initial, Vendor and Purspose Screens for the @lang language.', array('@lang' => strtoupper( $key ) ) ) . '</div>';
    }
    else {
      // Add language items to the inactive language items array.
      $inactive_lang_items[] = t('<a href="@url">Enable @lang Language</a>',
        array(
          '@url' => url('/admin/config/choice-cms/general', array('fragment' => 'edit-choice-lang' ) ),
          '@lang' => strtoupper( $key ),
        )
      ) . '<div class="description">' . t('The @lang can be enabled on the Choice CMS General Settings page.', array('@lang' => strtoupper( $key ) ) ) . '</div>';
    }
  }

  // Inital empty build array.
  $build = array();

  // Add the active container.
  $build['active'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'choice-cms-admin-section',
        'active-lang-list',
        'lang-list',
      )
    )
  );

  // Add the active header markup.
  $build['active']['header'] = array(
    '#type' => 'markup',
    '#markup' => '<h4>' . t( 'Active Choice CMS Languages' ) . '</h4>',
  );

  // Add the active list.
  $build['active']['list'] = array(
    '#theme'      => 'item_list',
    '#list_type'  => 'ul',
    '#items'      => $active_lang_items,
    '#attributes' => ['class' => 'admin-list'],
    '#weight'     => 5,
  );

  // Add the inactive container.
  $build['inactive'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'choice-cms-admin-section',
        'inactive-lang-list',
        'lang-list',
      )
    )
  );

  // Add the inactive header markup.
  $build['inactive']['header'] = array(
    '#type' => 'markup',
    '#markup' => '<h4>' . t( 'Inactive Choice CMS Languages' ) . '</h4>',
  );

  // Add the inactive list.
  $build['inactive']['list'] = array(
    '#theme' => 'item_list',
    '#list_type' => 'ul',
    '#items' => $inactive_lang_items,
    '#attributes' => ['class' => 'admin-list'],
    '#weight'
  );

  // Return the build with our active and inactive language items.
  return $build;

}
