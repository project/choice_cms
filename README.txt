README.txt for Choice CMS module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------

The Quantcast Choice plugin implements the Quantcast Choice GDPR Consent Tool.

IAB Europe announced a technical standard to support the digital advertising
ecosystem in meeting the General Data Protection Regulation (GDPR) consumer
consent requirements. This provides an implementation of that framework.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 - Install the Choice CMS module as you would normally install a contributed Drupal
   module. Visit https://help.quantcast.com/hc/en-us/categories/360000355134-Overview-for-Quantcast-Choice for further information.

AUTHOR/MAINTAINER
-----------------

 - Ryan Baron (rbaronqc) - https://www.quantcast.com
