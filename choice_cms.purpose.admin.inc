<?php

/**
 * @file
 * The Choice CMS Purpose Screen Settings functionality.
 */

/**
 * Quantcast Choice CMS Admin Settings Form.
 *
 * Purpose Screen Settings Form.
 *
 * @param $form
 *   An array contianing the form settings and field settings.
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @param $lang
 *   A string that contains the arg(4) element from the url, passed in from choice_cms_menu()
 * @return
 *   Drupal Form Array
 */
function choice_cms_settings_purpose_screen_form( $form, &$form_state, $lang ) {

  // Get the currently active Choice CMS items
  $active_languages = variable_get( 'choice_lang_active', array() );
  $lang = strtolower( $lang );

  // Purpose form definition.
  $form = array(
    '#attributes' => array(
      'class'     => array(
        'purpose-screen-form',
        'choice-cms',
        'waiting-for-values',
      ),
      'data-choice-lang' => strtolower( $lang ),
    ),
    '#attached' => array(
      'js'      => array(
        drupal_get_path('module', 'choice_cms') . '/js/admin/choice-cms.admin.min.js',
      )
    ),
  );

  // Add the form header section.
  $form['top'] = array(
    '#type'   => 'markup',
    '#markup' => '
      <div class="form-header">
        <h2>Purpose Screen Settings - ' . strtoupper( $lang ) . '</h2>
      </div>
    ',
    '#weight' => -1,
  );

  // If there are no active Choice CMS languages.
  if ( empty( $active_languages ) ) {

    // Add a message for display.
    $form['msg'] = array(
      '#type'   => 'markup',
      '#markup' => "<p>" . t( "You don't have any active Choice CMS languages selected." ) . "</p><p>" . t( "Head over to the <a href='@url'>Choice CMS General Settings</a> and activate your languages", array( "@url" => url( "/admin/config/choice-cms/general", array("fragment" => "edit-choice-lang" ) ) ) ) . "</p>",
      '#weight' => 2,
    );

    // Return the form with the no active languages message.
    return $form;

  }

  // The user has active languages, however the current page attempting to be viewed is not one of those active languages.
  elseif ( ! in_array( $lang, $active_languages, TRUE ) )  {

    $form['msg'] = array(
      '#type'   => 'markup',
      '#markup' => '<p>' . t( "@lang is not an active Choice CMS language.", array( '@lang' => strtoupper( $lang ) ) ) . '</p><p>' . t( 'Inactive Choice CMS languages can be activated on the <a href="@url">General Settings</a> page.', array( '@url' => url( '/admin/config/choice-cms/general', array('fragment' => 'edit-choice-lang' ) ) ) ) . '</p>',
      '#weight' => 2,
    );

    // The language being viewed is not an active language, return the form.
    return $form;

  }

  //////
  // The user is viewing a language settings page of a currently active Choice CMS language.
  //- Build the plugin settings for the Purpose screen.
  //////

  // Get the currently stored values.
  $choice_purpose_screen_header_title       = variable_get( 'choice_purpose_screen_header_title', json_encode( array() ) );
  $choice_purpose_screen_title              = variable_get( 'choice_purpose_screen_title', json_encode( array() ) );
  $choice_purpose_screen_body               = variable_get( 'choice_purpose_screen_body', json_encode( array() ) );
  $choice_purpose_screen_enable_all         = variable_get( 'choice_purpose_screen_enable_all', json_encode( array() ) );
  $choice_purpose_screen_disable_all        = variable_get( 'choice_purpose_screen_disable_all', json_encode( array() ) );
  $choice_purpose_screen_vendor             = variable_get( 'choice_purpose_screen_vendor', json_encode( array() ) );
  $choice_purpose_screen_cancel             = variable_get( 'choice_purpose_screen_cancel', json_encode( array() ) );
  $choice_purpose_screen_save_and_exit      = variable_get( 'choice_purpose_screen_save_and_exit', json_encode( array() ) );
  $choice_purpose_screen_pub_purpose_ids    = variable_get( 'choice_purpose_screen_pub_purpose_ids', json_encode( array() ) );

  // Decode the json returned from the above variable_get calls.
  $choice_purpose_screen_header_title       = isset( $choice_purpose_screen_header_title ) && ! empty( $choice_purpose_screen_header_title ) ? json_decode( $choice_purpose_screen_header_title, TRUE ) : array();
  $choice_purpose_screen_title              = isset( $choice_purpose_screen_title ) && ! empty( $choice_purpose_screen_title ) ? json_decode( $choice_purpose_screen_title, TRUE ) : array();
  $choice_purpose_screen_body               = isset( $choice_purpose_screen_body ) && ! empty( $choice_purpose_screen_body ) ? json_decode( $choice_purpose_screen_body, TRUE ) : array();
  $choice_purpose_screen_enable_all         = isset( $choice_purpose_screen_enable_all ) && ! empty( $choice_purpose_screen_enable_all ) ? json_decode( $choice_purpose_screen_enable_all, TRUE ) : array();
  $choice_purpose_screen_disable_all        = isset( $choice_purpose_screen_disable_all ) && ! empty( $choice_purpose_screen_disable_all ) ? json_decode( $choice_purpose_screen_disable_all, TRUE ) : array();
  $choice_purpose_screen_vendor             = isset( $choice_purpose_screen_vendor ) && ! empty( $choice_purpose_screen_vendor ) ? json_decode( $choice_purpose_screen_vendor, TRUE ) : array();
  $choice_purpose_screen_cancel             = isset( $choice_purpose_screen_cancel ) && ! empty( $choice_purpose_screen_cancel ) ? json_decode( $choice_purpose_screen_cancel, TRUE ) : array();
  $choice_purpose_screen_save_and_exit      = isset( $choice_purpose_screen_save_and_exit ) && ! empty( $choice_purpose_screen_save_and_exit ) ? json_decode( $choice_purpose_screen_save_and_exit, TRUE ) : array();
  $choice_purpose_screen_pub_purpose_ids    = isset( $choice_purpose_screen_pub_purpose_ids ) && ! empty( $choice_purpose_screen_pub_purpose_ids ) ? json_decode( $choice_purpose_screen_pub_purpose_ids, TRUE ) : array();

  // Add a loader to give the user feedback when the form is waiting for data.
  $form['loader'] = array(
    '#type'          => 'markup',
    '#markup'        => '<div class="loader"></div>',
    '#weight'        => -999,
  );

  // Add the current language to the form as a hidden field.
  $form['choice_purpose_screen']['lang'] = array(
    '#type'          => 'hidden',
    '#default_value' => $lang,
    '#weight'        => -100,
  );

  // Add Purpose Screen Header Title.
  $form['choice_purpose_screen']['header_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Header Title Text' ),
    '#description'   => t( 'The title text in the purposes header on the purposes consent page to be displayed in the ui.' ),
    '#default_value' => isset( $choice_purpose_screen_header_title[$lang] ) ? $choice_purpose_screen_header_title[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 5,
  );

  // Add Purpose Screen Title.
  $form['choice_purpose_screen']['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Title Text' ),
    '#description'   => t( 'The title text on the purposes consent page to be displayed in the ui.' ),
    '#default_value' => isset( $choice_purpose_screen_title[$lang] ) ? $choice_purpose_screen_title[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 10,
  );

  // Add Purpose Screen Body Text.
  $form['choice_purpose_screen']['body'] = array(
    '#type'          => 'textarea',
    '#title'         => t( 'Body Text' ),
    '#description'   => t( 'The main body text on the purposes consent page to be displayed in the ui.' ),
    '#default_value' => isset( $choice_purpose_screen_body[$lang] ) ? $choice_purpose_screen_body[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 15,
  );

  // Add Purpose Screen Enable All Button Text.
  $form['choice_purpose_screen']['enable_all'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Enable All Button Text' ),
    '#description'   => t( 'The enable all purpose consents button text on the purposes consent screen, displayed in the ui. When clicked, all of the purpose consent toggles are set to on.' ),
    '#default_value' => isset( $choice_purpose_screen_enable_all[$lang] ) ? $choice_purpose_screen_enable_all[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 20,
  );

  // Add Purpose Screen Disable All Button Text.
  $form['choice_purpose_screen']['disable_all'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Disable All Button Text' ),
    '#description'   => t( 'The disable all purpose consents button text on the purposes consent screen, displayed in the ui. When clicked, all of the purpose consent toggles are set to off.' ),
    '#default_value' => isset( $choice_purpose_screen_disable_all[$lang] ) ? $choice_purpose_screen_disable_all[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 22,
  );

  // Add Purpose Screen Vendor Link Text.
  $form['choice_purpose_screen']['vendor'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Vendor Link Text' ),
    '#description'   => t( 'The vendor link text displayed on the Purpose Screen.' ),
    '#default_value' => isset( $choice_purpose_screen_vendor[$lang] ) ? $choice_purpose_screen_vendor[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 25,
  );

  // Add Purpose Screen Cancel Button Text.
  $form['choice_purpose_screen']['cancel'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Cancel Button Text' ),
    '#description'   => t( 'The cancel button text displayed on the Purpose Screen.' ),
    '#default_value' => isset( $choice_purpose_screen_cancel[$lang] ) ? $choice_purpose_screen_cancel[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 30,
  );

  // Add Purpose Screen Save and Exit Button Text.
  $form['choice_purpose_screen']['save_and_exit'] = array(
    '#type'          => 'textfield',
    '#title'         => t( 'Save and Exit Button Text' ),
    '#description'   => t( 'The save consent and exit the ui button text on the purposes consent page to be displayed in the ui.' ),
    '#default_value' => isset( $choice_purpose_screen_save_and_exit[$lang] ) ? $choice_purpose_screen_save_and_exit[$lang] : '',
    '#required'      => TRUE,
    '#field_suffix'  => '',
    '#weight'        => 35,
  );

  // Add Purpose Screen Vendor Consents Purposes to ask for.
  $form['choice_purpose_screen']['pub_purpose_ids'] = array(
    '#type'         => 'checkboxes',
    '#title'        => t( 'Publisher Consents Purposes' ),
    '#field_suffix' => '',
    '#options'      => array(
      '1'             => t( "Allow storing or accessing information on a user's device." ),
      '2'             => t( "Allow processing of a user's data to provide and inform personalised advertising (including delivery, measurement, and reporting) based on a user's preferences or interests known or inferred from data collected across multiple sites, apps, or devices; and/or accessing or storing information on devices for that purpose." ),
      '3'             => t( "Allow processing of a user's data to deliver content or advertisements and measure the delivery of such content or advertisements, extract insights and generate reports to understand service usage; and/or accessing or storing information on devices for that purpose." ),
      '4'             => t( "Allow processing of a user's data to provide and inform personalised content (including delivery, measurement, and reporting) based on a user's preferences or interests known or inferred from data collected across multiple sites, apps, or devices; and/or accessing or storing information on devices for that purpose." ),
      '5'             => t( "Allow processing of a user's data for measurement purposes." ),
    ),
    '#default_value' => isset( $choice_purpose_screen_pub_purpose_ids[$lang] ) ? $choice_purpose_screen_pub_purpose_ids[$lang] : array(),
    '#required'      => TRUE,
    '#weight'      => 40,
  );

  // Add the form submit button.
  $form['submit_button'] = array(
    '#type'   => 'submit',
    '#value'  => t( "Update Purpose Screen Settings" ) . " - " . strtoupper( $lang ),
    '#weight' => 1000,
  );

  return $form;

}

/**
 * Implements hook_submit().
 */
function choice_cms_settings_purpose_screen_form_submit( $form, &$form_state ) {

  $active_languages = variable_get( 'choice_lang_active', array() );
  $values           = isset( $form_state['values'] ) && ! empty( isset( $form_state['values'] ) ) ? $form_state['values'] : array();

  // Get the current stored values for the purpose screen
  $current_choice_purpose_screen_header_title      = json_decode( variable_get( 'choice_purpose_screen_header_title', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_title             = json_decode( variable_get( 'choice_purpose_screen_title', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_body              = json_decode( variable_get( 'choice_purpose_screen_body', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_enable_all        = json_decode( variable_get( 'choice_purpose_screen_enable_all', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_disable_all       = json_decode( variable_get( 'choice_purpose_screen_disable_all', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_vendor            = json_decode( variable_get( 'choice_purpose_screen_vendor', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_cancel            = json_decode( variable_get( 'choice_purpose_screen_cancel', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_save_and_exit     = json_decode( variable_get( 'choice_purpose_screen_save_and_exit', json_encode( array() ) ), TRUE );
  $current_choice_purpose_screen_pub_purpose_ids   = json_decode( variable_get( 'choice_purpose_screen_pub_purpose_ids', json_encode( array() ) ), TRUE );

  // Get the values submitted for the intitial screen form
  $lang                                            = isset( $values['lang'] )            ? $values['lang']            : '';
  $submitted_choice_purpose_screen_header_title    = isset( $values['header_title'] )    ? $values['header_title']    : '';
  $submitted_choice_purpose_screen_title           = isset( $values['title'] )           ? $values['title']           : '';
  $submitted_choice_purpose_screen_body            = isset( $values['body'] )            ? $values['body']            : '';
  $submitted_choice_purpose_screen_enable_all      = isset( $values['enable_all'] )      ? $values['enable_all']      : '';
  $submitted_choice_purpose_screen_disable_all     = isset( $values['disable_all'] )     ? $values['disable_all']     : '';
  $submitted_choice_purpose_screen_vendor          = isset( $values['vendor'] )          ? $values['vendor']          : '';
  $submitted_choice_purpose_screen_cancel          = isset( $values['cancel'] )          ? $values['cancel']          : '';
  $submitted_choice_purpose_screen_save_and_exit   = isset( $values['save_and_exit'] )   ? $values['save_and_exit']   : '';
  $submitted_choice_purpose_screen_pub_purpose_ids = isset( $values['pub_purpose_ids'] ) ? $values['pub_purpose_ids'] : '';
  // Only save values if the language being saved is an active language.

  if ( in_array( strtolower( $lang ), $active_languages ) )  {

    // Update the current values, for the specific language submitted
    // Encode the array into json and save the field data
    $current_choice_purpose_screen_header_title[$lang] = $submitted_choice_purpose_screen_header_title;
    variable_set( 'choice_purpose_screen_header_title', json_encode( $current_choice_purpose_screen_header_title ) );

    $current_choice_purpose_screen_title[$lang] = $submitted_choice_purpose_screen_title;
    variable_set( 'choice_purpose_screen_title', json_encode( $current_choice_purpose_screen_title ) );

    $current_choice_purpose_screen_body[$lang] = $submitted_choice_purpose_screen_body;
    variable_set( 'choice_purpose_screen_body', json_encode( $current_choice_purpose_screen_body ) );

    $current_choice_purpose_screen_enable_all[$lang] = $submitted_choice_purpose_screen_enable_all;
    variable_set( 'choice_purpose_screen_enable_all', json_encode( $current_choice_purpose_screen_enable_all ) );

    $current_choice_purpose_screen_disable_all[$lang] = $submitted_choice_purpose_screen_disable_all;
    variable_set( 'choice_purpose_screen_disable_all', json_encode( $current_choice_purpose_screen_disable_all ) );

    $current_choice_purpose_screen_vendor[$lang] = $submitted_choice_purpose_screen_vendor;
    variable_set( 'choice_purpose_screen_vendor', json_encode( $current_choice_purpose_screen_vendor ) );

    $current_choice_purpose_screen_cancel[$lang] = $submitted_choice_purpose_screen_cancel;
    variable_set( 'choice_purpose_screen_cancel', json_encode( $current_choice_purpose_screen_cancel ) );

    $current_choice_purpose_screen_save_and_exit[$lang] = $submitted_choice_purpose_screen_save_and_exit;
    variable_set( 'choice_purpose_screen_save_and_exit', json_encode( $current_choice_purpose_screen_save_and_exit ) );

    $current_choice_purpose_screen_pub_purpose_ids[$lang] = $submitted_choice_purpose_screen_pub_purpose_ids;
    variable_set( 'choice_purpose_screen_pub_purpose_ids', json_encode( $current_choice_purpose_screen_pub_purpose_ids ) );

  }

}
